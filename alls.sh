#!/bin/bash

cd "${BASH_SOURCE%/*}" || exit

printf "\n"
./usage.sh
printf "\n"
./temperature.sh
printf "\n"
./services.sh
printf "\n"
./processes.sh
printf "\n"
./update.sh
./domain_expiration.sh
printf "\n"
./logs.sh
printf "\n"

