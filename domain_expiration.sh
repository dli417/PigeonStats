#!/bin/bash

EXPIRATION_DATE="Jul 24, 2020"
DOMAIN_NAME="teaso.me"
clear="$(tput sgr0)"
echo "$(tput smul)$DDOMAIN_NAME$clear domain name expires in $(tput setaf 3)$((($(date -d "$EXPIRATION_DATE" +%s) - $(date +%s)) / 86400))$clear days. $(tput dim)(namecheap.com)$(tput sgr0)";

