#!/bin/bash

clear=$(tput sgr0)
inactive="$(tput setaf 1)▲ inactive$clear"
active="$(tput setaf 2)● active$clear"

ufw=$inactive
mian=$inactive
caddy=$inactive
mariadb=$inactive
nextcloud=$inactive
laverna=$inactive
ackee=$inactive

printf "%s\n\n" "$(tput bold)Services$clear"

systemctl is-active --quiet ufw && ufw=$active


web_health () {
  [ "$(curl --http2 --silent --output /dev/null --head -w '%{http_code}' "$1")" -eq "${2:-200}" ]
}

# caddy doesn't support HEAD request anymore, can't use curl's --head flag
# https://github.com/mholt/caddy/commit/4f8ff095514b75fc72a54be36d2e3d923db97988
web_health https://pro.teaso.me/ && caddy=$active
web_health https://cloud.teaso.me/status.php && nextcloud=$active
web_health https://ackee.teaso.me/tracker.js 204 && ackee=$active # returns 204 "no content" if you use curl --head
mysql --execute="SELECT 1" --silent --quick > /dev/null 2>/dev/null && mariadb=$active

printf " Firewall:   %24s  MariaDB:   %24s\n" "$ufw"           "$mariadb"
printf " NextCloud:  %24s  Ackee:     %24s\n" "$nextcloud"     "$ackee"

