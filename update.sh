#!/bin/bash

uptime --pretty

clear=$(tput sgr0)
last_pac=$(tac /var/log/pacman.log | grep -m1 -F "[PACMAN] starting full system upgrade" | cut -d "[" -f2 | cut -d "]" -f1)
time_since=$((($(date +%s)-$(date --date="$last_pac" +%s))/3600))
echo "It has been $(tput bold)$time_since hour$([ $time_since -ne 1 ] && echo s)$clear since your last $(tput setaf 6)pacman -Syu$clear"





function check_update_from_github () {
  local repo=$1
  local current_version=$2
  local latest_version=$(curl -s https://api.github.com/repos/$repo/releases/latest | jq --raw-output .name)
  if [ "$current_version" != "$latest_version" ]; then
    echo "A newer version for $(tput bold)$repo$(tput sgr0) is available on Github."
    current_version="$(tput setaf 1)$current_version$(tput sgr0) $(tput dim)(current)$(tput sgr0)"
    latest_version="$(tput setaf 2)$latest_version$(tput sgr0) $(tput dim)(latest)$(tput sgr0)"
    echo "$current_version --> $latest_version"
  fi
}

check_update_from_github usefathom/fathom "v1.2.1"
check_update_from_github electerious/Ackee "v1.7.1"

